def timer(fun):
	import time
	def wrapper(*args, **kwargs):
		t1=time.time()
		result=fun(*args, **kwargs)
		tooktime=time.time()-t1
		print ("your {} algorithm took {} time".format(fun.__name__, tooktime))
	return wrapper
