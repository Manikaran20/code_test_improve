n,m=list(map(int,input().split(" ")))

arr=[0]*(n+1)

for i in range(0,m):
    a,b,k=list(map(int,input().split(" ")))
    arr[a-1]+=k
    if b<=(n+1):
        arr[b]-=k

max=sum=0

for each in arr:
    sum+=each

    if sum>max:
        max=sum

        
print(max)