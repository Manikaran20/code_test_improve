#Code to check if a given string is a pallindrome or not.
from checkPerformance import timer

@timer
def pallindrome(string):
	flag=False
	l=len(string)

	for i, j in zip(range(0,l/2), range(-1,-l/2,-1)):
		if(string[i]==string[j]):
			flag=True
		else:
			flag=False

	if(flag==True):
		return ("Yes")
	else:
		return ("No")

if ('__main__'==__name__):
	pallindrome(input())