import unittest
from pallindrome import pallindrome

class TestPallindrome(unittest.TestCase):
	def test_yes_pall(self):
		string='abcdefghhgfedcba'
		self.assertEqual(pallindrome(string), "Yes")
	def test_no_pall(self):
		string='kdjncj'
		self.assertEqual(pallindrome(string),"No")

if __name__=='__main__':
	unittest.main()