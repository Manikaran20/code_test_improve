def prime(num):
    if num in [2,3,5,7]:
        return True
    elif num==1 or num%2==0:
        return False
    elif num%3==0:
        return False
    elif num%5==0:
        return False
    elif num%7==0:
        return False
    else:
        i=3
        while(i*i<=num):
            if num%i==0:
                return False
            i+=2
        return True
    

n=int(input())
for _ in range(n):
    isprime=prime(int(input()))
    if (isprime):
        print("Prime")
    else:
        print("Not prime")
