import concurrent.futures
import time
import pdb

def myfunc(name):
	print (f"myfunc started with {name}")
	time.sleep(5)
	print (f"myfunc ended with {name}")
if __name__=="__main__":
	print ("main started")
	with concurrent.futures.ThreadPoolExecutor(max_workers=3) as e:
	
		e.map(myfun, ['mani', 'karan', 'singh'])
		pdb.set_trace()
		#breakpoint()