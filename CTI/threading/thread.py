import threading
import time

def myfunc(name):
	print (f"my func started with {name}")
	time.sleep(5)
	print ("myfunc ended")

if __name__=='__main__':
	print ("main started")
	t=threading.Thread(target=myfunc, args=["mypython"])
	t.start()
	print ("main ended before myfunc executes")
	t.join()
	print ("myfunc executes first and then main ends")