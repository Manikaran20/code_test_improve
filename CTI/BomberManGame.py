

import math
import os
import random
import re
import sys

# Complete the bomberMan function below.

"""Bomberman lives in a rectangular grid. Each cell in the grid either contains a bomb or nothing at all.

Each bomb can be planted in any cell of the grid but once planted, it will detonate after exactly 3 seconds. Once a bomb detonates, it's destroyed — along with anything in its four neighboring cells. This means that if a bomb detonates in cell
, any valid cells and

are cleared. If there is a bomb in a neighboring cell, the neighboring bomb is destroyed without detonating, so there's no chain reaction.

Bomberman is immune to bombs, so he can move freely throughout the grid. Here's what he does:

    Initially, Bomberman arbitrarily plants bombs in some of the cells, the initial state.
    After one second, Bomberman does nothing.
    After one more second, Bomberman plants bombs in all cells without bombs, thus filling the whole grid with bombs. No bombs detonate at this point.
    After one more second, any bombs planted exactly three seconds ago will detonate. Here, Bomberman stands back and observes.
    Bomberman then repeats steps 3 and 4 indefinitely.

Note that during every second Bomberman plants bombs, the bombs are planted simultaneously (i.e., at the exact same moment), and any bombs planted at the same time will detonate at the same time.

Given the initial configuration of the grid with the locations of Bomberman's first batch of planted bombs, determine the state of the grid after
seconds. """

def bomberMe(grid,n,t):
    for _ in range(t):
        brid=[]
        for _ in range(len(grid)):
            brid.append(['P']*len(grid[0]))
        li,lj=len(grid),len(grid[0])
        for i in range(li):
                grid[i]=list(grid[i])
                for j in range(lj):
                    if grid[i][j]=='O':
                        brid[i][j]='.'
                        print ('a',i,j)
                        continue
                    try:
                        if grid[i+1][j]=='O':
                            brid[i][j]='.'
                            print ('b',i,j)
                            continue
                    except:
                        pass
                    try:
                        if i!=0:
                            if grid[i-1][j]=='O':
                                brid[i][j]='.'
                                print ('c', i,j)
                                continue
                    except:
                        pass
                    try:
                        if grid[i][j+1]=='O':
                            brid[i][j]='.'
                            print ('d',i,j)
                            continue
                    except:
                        pass
                    try:
                        if j!=0:
                            if grid[i][j-1]=='O':
                                brid[i][j]='.'
                                print ('e',i,j)
                                continue
                    except:
                        pass
                    brid[i][j]='O'
        grid=brid
    return (grid)
def bomberMan(n, grid):
    if n==1:
        return (grid)
    if n%2==0:
        l=len(grid)
        m=len(grid[0])
        grid=[]
        for _ in range(l):
            grid.append('O'*m)
        return grid
    else:
        if n in range(3,10**9,4):
            return list(map(lambda x: ''.join(x), bomberMe(grid,n,1)))
        else:
            return list(map(lambda x: ''.join(x),bomberMe(grid,n,2)))
    
            




if __name__ == '__main__':
    fptr = open(os.environ['OUTPUT_PATH'], 'w')

    rcn = input().rstrip().split()

    r = int(rcn[0])

    c = int(rcn[1])

    n = int(rcn[2])

    grid = []

    for _ in range(r):
        grid_item = input()
        grid.append(grid_item)

    result = bomberMan(n, grid)
