def bss(string):
	max_len=0
	for i in range(len(string)-1):
		s=string[i]
		repeated=[s]
		for j in range(i+1, len(string)):
			if string[j] in repeated:
				break
			s+=string[j]
			repeated.append(string[j])
			if len(s)>max_len:
				max_len=len(s)
				bs=s
	return (bs)

if __name__=='__main__':
	print (bss(input()))